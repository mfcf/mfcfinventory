#!/usr/bin/python3

from pprint import pprint
from mfcfinventory import Inventory

import datetime
import logging

logger = logging.getLogger(__name__)


from test_config import *

try:
    from test_config_local import *
except ImportError:
    pass


inv = Inventory(**Inventory_kargs)


a = {
    "pKey": 16458,
    "inv_mac": [{"macAddress": "00:50:56:84:6d:44", "macName": "eth0_vlan206"}],
    "dns_admin": "dns-math-admin@uwaterloo.ca",
    "special": "Normal",
    "operatingSystem": "Linux",
    "pingDate": datetime.date(2017, 1, 20),
    "equipmentType": "Computer-Virtual",
    "comments": "salt managed record",
    "supportClass": "All",
    "numberOfCPUs": 4,
    "sponsorCode": "Math-Tech600",
    "policyEight": "Restricted",
    "purpose": "gitlab-ci runner server for running continuous integration test",
    "groups": "mfcf-staff",
    "memory": "4 GB",
    "active": "y",
    "entered": datetime.date(2017, 1, 20),
    "inv_dns": [
        {
            "hostDomainName": "ci-p01-01",
            "domain": "uwaterloo.ca",
            "ipAddress": "129.97.206.17",
            "ipPurpose": "primary",
            "macAddress": "00:50:56:84:6d:44",
        }
    ],
    "unit": "MATH",
    "dns_contact": "dns-math-contact@uwaterloo.ca",
}

b = {
    "cpuModel": None,
    "warrantyStop": None,
    "numberOfCores": None,
    "contractAFF": None,
    "adminContact": None,
    "speed": None,
    "unit": "MATH",
    "contractCost": None,
    "fixedAssetTag": None,
    "sponsorCode": "Math-Tech600",
    "comments": "salt managed record",
    "supportClass": "All",
    "numberOfCPUs": 4,
    "contractQuoteNumber": None,
    "special": "Normal",
    "memory": "4 GB",
    "warrantyStart": None,
    "authUser": None,
    "vendor": None,
    "accountNumber": None,
    "contractPurchaseOrder": None,
    "purchaseCost": None,
    "barcode": None,
    "policyEight": "Restricted",
    "purpose": "gitlab-ci runner server for running continuous integration test",
    "groups": "mfcf-staff",
    "contractBegins": None,
    "inv_mac": [{"macAddress": "00:50:56:84:6d:44", "macName": "eth0_vlan206"}],
    "dns_admin": "dns-math-admin@uwaterloo.ca",
    "room": None,
    "contractVendor": None,
    "serialNumber": None,
    "equipmentType": "Computer-Virtual",
    "operatingSystem": "Linux",
    "contractEnds": None,
    "purchaseOrder": None,
    "inv_dns": [
        {
            "hostDomainName": "ci-p01-01",
            "domain": "uwaterloo.ca",
            "ipAddress": "129.97.206.17",
            "ipPurpose": "primary",
            "macAddress": "00:50:56:84:6d:44",
        }
    ],
    "found": None,
    "model": None,
    "processor": None,
    "dns_contact": "dns-math-contact@uwaterloo.ca",
}


pprint(
    inv.find_record_changes(
        a,
        b,
        verbose_level=1,
        strip_empty=True,
        populate_inventory_rw_item=True,
        strip_inventory_ro_item=True,
    )
)


a = {
    "pKey": 16459,
    "inv_mac": [{"macAddress": "00:50:56:84:64:9a", "macName": "eth0_vlan206"}],
    "dns_admin": "dns-math-admin@uwaterloo.ca",
    "special": "Normal",
    "operatingSystem": "Linux",
    "pingDate": datetime.date(2017, 1, 20),
    "equipmentType": "Computer-Virtual",
    "comments": "salt managed record",
    "supportClass": "All",
    "numberOfCPUs": 4,
    "sponsorCode": "Math-Tech600",
    "policyEight": "Restricted",
    "purpose": "gitlab-ci runner server for running continuous integration test",
    "groups": "mfcf-staff",
    "memory": "4 GB",
    "active": "y",
    "entered": datetime.date(2017, 1, 20),
    "inv_dns": [
        {
            "hostDomainName": "ci-p01-02",
            "domain": "uwaterloo.ca",
            "ipAddress": "129.97.206.18",
            "ipPurpose": "primary",
            "macAddress": "00:50:56:84:64:9a",
        }
    ],
    "unit": "MATH",
    "dns_contact": "dns-math-contact@uwaterloo.ca",
}

b = {
    "cpuModel": None,
    "warrantyStop": None,
    "numberOfCores": None,
    "contractAFF": None,
    "adminContact": None,
    "speed": None,
    "unit": "MATH",
    "contractCost": None,
    "fixedAssetTag": None,
    "sponsorCode": "Math-Tech600",
    "comments": "salt managed record",
    "supportClass": "All",
    "numberOfCPUs": 4,
    "contractQuoteNumber": None,
    "special": "Normalx",
    "memory": "4 GB",
    "warrantyStart": None,
    "authUser": None,
    "vendor": None,
    "accountNumber": None,
    "contractPurchaseOrder": None,
    "purchaseCost": None,
    "barcode": None,
    "policyEight": "Restricted",
    "purpose": "gitlab-ci runner server for running continuous integration test",
    "groups": "mfcf-staff",
    "contractBegins": None,
    "inv_mac": {"00:50:56:84:64:9a": {"macName": "eth0_vlan206"}},
    "dns_admin": "dns-math-admin@uwaterloo.ca",
    "room": None,
    "contractVendor": None,
    "serialNumber": None,
    "equipmentType": "Computer-Virtual",
    "operatingSystem": "Linux",
    "contractEnds": None,
    "purchaseOrder": None,
    "inv_dns": {
        "ci-p01-02.uwaterloo.ca": {
            "hostDomainName": "ci-p01-02",
            "domain": "uwaterloo.ca",
            "ipAddress": "129.97.206.18",
            "ipPurpose": "primary",
            "macAddress": "00:50:56:84:64:9a",
        }
    },
    "found": None,
    "model": None,
    "processor": None,
    "dns_contact": "dns-math-contact@uwaterloo.ca",
}


pprint(
    inv.find_record_changes(
        a,
        b,
        ignore_order=True,
        verbose_level=3,
        strip_empty=True,
        populate_inventory_rw_item=True,
        strip_inventory_ro_item=True,
    )
)


"""

d = inv.get(pkey='16432')
pprint(d)


gettest = inv.get(barcode='MF923456')
print('GET BARCODE-------------')
pprint(gettest)

gettest = inv.get(host='influxdb-p01.math')
print('GET BARCODE-------------')
pprint(gettest)


d = {'accountNumber': '',
 'active': 'y',
 'equipmentType': 'Computer-Virtual',
 'groups': 'arg-fe',
 'inv_dns': {'opencs-fe-p01-01.uwaterloo.ca': {'domain': 'uwaterloo.ca',
                                               'hostDomainName': 'opencs-fe-p01-01',
                                               'ipAddress': '129.97.206.15',
                                               'ipPurpose': 'front end proxy to app servers',
                                               'macAddress': '00:50:56:84:65:8c'}},
 'inv_mac': {'00:50:56:84:65:8c': {'macName': 'test'}},
 'operatingSystem': 'Linux',
 'policyEight': 'Restricted',
 'special': 'Normal',
 'sponsorCode': 'Math-Tech100',
 'supportClass': 'All',
 'unit': 'MATH'}


'''
db = inv.get_db()
cursor = db.cursor()
cursor.execute("DELETE FROM history WHERE userName = 'sqluser_mfcfsalt' and pkey = 15646")
'''



createtest = {
    'accountNumber': '',
    'unit': 'MATH',
    'barcode': 'MF923456',
    'equipmentType': 'Computer-Virtual',
    'groups': 'mfcf-test',
    'special': 'Normal',
    'operatingSystem':'linux',
    'policyEight': 'Restricted',
    'purpose': 'testing libinventory',
    'dns_admin': 's8weber@uwaterloo.ca',
    'dns_contact': 's8weber@uwaterloo.ca',
    'inv_mac': {
        '99:25:90:5a:79:2c': { 'macName': 'eth0' },
        '99:25:90:5a:79:4c': { 'macName': 'eth1' },
    },
    'inv_dns': {
        'test_exampleMF9999998.uwaterloo.ca': {
            'hostDomainName': 'test_exampleMF9999998',
            'domain': 'uwaterloo.ca',
            'ipAddress': '127.0.0.9',
            'ipPurpose': 'test MF9999998',
            'macAddress': '99:25:90:5a:79:2c',
        },
    },
}
rowid = inv.create(createtest)
print('rowid-------'); print(rowid)

gettest = inv.get(barcode='MF923456')
print('GET BARCODE-------------')
pprint(gettest)


print('DIFF-----------')

createtest_change = {
    'accountNumber': '',
    'unit': 'MATH',
    'barcode': 'MF923456',
    'equipmentType': 'Computer-Virtual',
    'groups': 'mfcf-test',
    'special': 'Normal',
    'operatingSystem':'linux',
    'policyEight': 'Restricted',
    'purpose': 'testing',
    'dns_admin': 's8weber@uwaterloo.ca',
    'dns_contact': 's8weber@uwaterloo.ca',
    'inv_mac': {
        '99:25:90:5a:79:2c': { 'macName': 'eth0' },
        '99:25:90:5a:79:4c': { 'macName': 'eth1' },
    },
    'inv_dns': {
        'test_exampleMF9.uwaterloo.ca': {
            'hostDomainName': 'test_exampleMF9999998',
            'domain': 'uwaterloo.ca',
            'ipAddress': '127.0.0.9',
            'ipPurpose': 'test MF9999998',
            'macAddress': '99:25:90:5a:79:2c',
        },
    },
}

changes = inv.find_record_changes(gettest, createtest_change)
if changes:
    pprint(changes)

print('UPDATE-----------')
changes = inv.update(gettest['pKey'], createtest_change)
if changes:
    pprint(changes)

# confirmed.
print('DELETE-----------')
inv.delete(gettest['pKey'])


'''
## network is a helper that support extras like cname and improved field names
    ## if network is defined inv_dns and inv_mac are ignored
    '_network': {
        'mac': {
            '99:25:90:5a:79:2c': { 'comment': 'eth0' },
            '99:25:90:5a:79:4c': { 'comment': 'eth1' },
        },
        'host': {
            'test_exampleMF9999998.uwaterloo.ca': {
                'mac': '99:25:90:5a:79:2c',
                'ipv4addr': '127.0.0.9',
                'comment': 'test MF9999998',
            },
        },
        'cname': {
            'test_exampleMF.uwaterloo.ca': {
                'canonical': 'test_exampleMF9999998.uwaterloo.ca',
                'comment': 'test cname',
            },
        },
    }

                'hostDomainName': dns['hostDomainName'],
                'domain': dns['domain'],
                'ipPurpose': dns['ipPurpose'],
                'ipAddress': dns['ipAddress'],
                'macAddress': macAddress,
'''



"""
