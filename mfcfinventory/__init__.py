import logging

logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())


# ========================================================================================

"""
Dictionary with auto-expiring values for caching purposes.

Expiration happens on any access, object is locked during cleanup from expired
values. Can not store more than max_len elements - the oldest will be deleted.

>>> ExpiringDict(max_len=1000, max_age_seconds=2000, age_random_gitter=30)

The values stored in the following way:
{
    key1: (value1, created_time1),
    key2: (value2, created_time2)
}
NOTE: iteration over dict and also keys() do not remove expired values!
"""

import time
from threading import RLock
import random

try:
    from collections import OrderedDict
except ImportError:
    # Python < 2.7
    from ordereddict import OrderedDict


class ExpiringDict(OrderedDict):
    def __init__(self, max_len, max_age_seconds, age_random_gitter):
        OrderedDict.__init__(self)
        self.max_len = max_len
        self.max_age = max_age_seconds
        self.age_random_gitter = age_random_gitter
        self.lock = RLock()

    def __contains__(self, key):
        """Return True if the dict has a key, else return False."""
        try:
            with self.lock:
                item = OrderedDict.__getitem__(self, key)
                if time.time() - item[1] < self.max_age:
                    return True
                else:
                    del self[key]
        except KeyError:
            pass
        return False

    def __getitem__(self, key, with_age=False):
        """Return the item of the dict.

        Raises a KeyError if key is not in the map.
        """
        with self.lock:
            item = OrderedDict.__getitem__(self, key)
            item_age = time.time() - item[1]
            if item_age < self.max_age:
                if with_age:
                    return item[0], item_age
                else:
                    ## TODO: deep copy needed?
                    return item[0]
            else:
                del self[key]
                raise KeyError(key)

    def __setitem__(self, key, value):
        """Set d[key] to value."""
        with self.lock:
            if len(self) == self.max_len:
                self.popitem(last=False)
            rnd_gitter = 0
            if self.age_random_gitter > 0:
                rnd_gitter = random.randint(0, self.age_random_gitter)
            OrderedDict.__setitem__(self, key, (value, time.time() + rnd_gitter))

    def pop(self, key, default=None):
        """Get item from the dict and remove it.

        Return default if expired or does not exist. Never raise KeyError.
        """
        with self.lock:
            try:
                item = OrderedDict.__getitem__(self, key)
                del self[key]
                return item[0]
            except KeyError:
                return default

    def ttl(self, key):
        """Return TTL of the `key` (in seconds).

        Returns None for non-existent or expired keys.
        """
        key_value, key_age = self.get(key, with_age=True)
        if key_age:
            key_ttl = self.max_age - key_age
            if key_ttl > 0:
                return key_ttl
        return None

    def get(self, key, default=None, with_age=False):
        "Return the value for key if key is in the dictionary, else default."
        try:
            return self.__getitem__(key, with_age)
        except KeyError:
            if with_age:
                return default, None
            else:
                return default

    def items(self):
        """Return a copy of the dictionary's list of (key, value) pairs."""
        r = []
        for key in self:
            try:
                r.append((key, self[key]))
            except KeyError:
                pass
        return r

    def values(self):
        """Return a copy of the dictionary's list of values.
        See the note for dict.items()."""
        r = []
        for key in self:
            try:
                r.append(self[key])
            except KeyError:
                pass
        return r

    def fromkeys(self):
        raise NotImplementedError()

    def iteritems(self):
        raise NotImplementedError()

    def itervalues(self):
        raise NotImplementedError()

    def viewitems(self):
        raise NotImplementedError()

    def viewkeys(self):
        raise NotImplementedError()

    def viewvalues(self):
        raise NotImplementedError()


# ========================================================================================


# sudo apt-get install python-pip python-dev libmysqlclient-dev
# apt-get install python-mysqldb
# pacman -Syu python-pip
# pip3 install mysqlclient or perhaps PyMySQL
# mysqlclient has good suport for python3
# sudo pacman -Su mysql-python


#import MySQLdb
import mysql.connector
import json
import datetime
from deepdiff import DeepDiff


knowen_domains = [
    "mathtest.uwaterloo.ca",
    "cloud.cs.uwaterloo.ca",
    "math.private.uwaterloo.ca",
    "lib.uwaterloo.ca",
    "mathsoc.uwaterloo.ca",
    "student.cs.private.uwaterloo.ca",
    "esx.uwaterloo.ca",
    "ads.uwaterloo.ca",
    "cscf.uwaterloo.ca",
    "cs.private.uwaterloo.ca",
    "private.uwaterloo.ca",
    "mfcf.uwaterloo.ca",
    "cs.uwaterloo.ca",
    "math.ctx,uwaterloo.ca",
    "uwaterloo.ca",
    "cgi.uwaterloo.ca",
    "mathroottest.uwaterloo.ca",
    "storage.cs.uwaterloo.ca",
]


def parse_domainname(fqdn, domains):
    """
    Get host domain name

    If domains are provided, the best match out of the list is returned.
    If none are found the return is None
    """
    fqdn = fqdn.lower()
    match = ""
    for d in domains:
        d = d.lower()
        if fqdn.endswith(d) and len(d) > len(match):
            match = d
    if len(match) > 0:
        return match


def parse_hostname(fqdn, domains):
    """
    Get hostname

    If domains are provided, the best match out of the list is truncated from the fqdn leaving the hostname
    If no domains are found the fqdn is returned
    """
    fqdn = fqdn.lower()
    domain = parse_domainname(fqdn, domains)
    if domain and domain in fqdn:
        return fqdn.rsplit("." + domain)[0]
    return fqdn


class Inventory:
    host = ""
    user = ""
    passwd = ""
    database = ""
    cache = ExpiringDict(max_len=1500, max_age_seconds=900, age_random_gitter=30)
    db = None

    inventory_ro_items = ["pKey", "entered", "pingDate"]
    inventory_rw_items = [
        "active",
        "isSaltProvisioned",
        "policyEight",
        "numberOfCPUs",
        "warrantyStop",
        "cpuModel",
        "dns_contact",
        "comments",
        "supportBegins",
        "hardware",
        "purchaseOrder",
        "speed",
        "description",
        "warrantyStart",
        "room",
        "groups",
        "sponsorCode",
        "parentBarcode",
        "fixedAssetTag",
        "authUser",
        "serialNumber",
        "systemDescription",
        "edocsUrl",
        "processor",
        "memory",
        "vendor",
        "contractEnds",
        "subscriptionCode",
        "physicalAddressDate",
        "unit",
        "contractAFF",
        "adminContact",
        "dns_admin",
        "supportEnds",
        "physicalAddress",
        "accountNumber",
        "equipmentType",
        "special",
        "contractBegins",
        "purchaseCost",
        "systemDescriptionDate",
        "purpose",
        "contractQuoteNumber",
        "operatingSystem",
        "contractCost",
        "supportClass",
        "lastModified",
        "model",
        "contractVendor",
        "contractPurchaseOrder",
        "region",
        "barcode",
        "supportAFF",
        "operatingSystemSNMP",
        "numberOfCores",
    ]
    inventory_rw_item_dict = {a: None for a in inventory_rw_items}

    types = {}

    def __init__(self, host="xxx", user="xxx", passwd="xxx", database="xxx"):
        self.host = host
        self.user = user
        self.passwd = passwd
        self.database = database
        # populate what some valid types are
        self.types["domain"] = self.get_domains()
        self.types["equipment"] = self.get_equipment_types()
        self.types["group"] = self.get_groups()
        self.types["special"] = self.get_specials()
        self.types["support"] = self.get_support_classes()
        self.types["sponsor"] = self.get_sponsors()

    def _get_db(self):
        if self.db:
            try:
                # check db connection
                self.db.cursor(buffered=True).execute("SELECT 1;")
            except Exception as ex:
                logger.warning(ex)
                # _mysql_exceptions.OperationalError: (2006, 'MySQL server has gone away')
                self.db = None
                logger.debug("database connection set to restart")
        if not self.db:
            # recreate connection
            self.db = mysql.connector.connect(host=self.host, user=self.user, password=self.passwd, database=self.database)
            #self.db.autocommit(True)
            self.db.autocommit = True
            logger.debug("connected to database")
        return self.db

    def _insert(self, table, data):
        """
        this fuction handles some of the mess inserting data
        # NOTE: dict keys not safe from sql injection however values are safe
        """
        cursor = self._get_db().cursor()
        fields = ", ".join(data.keys())
        fields_values = ", ".join(["%({0})s".format(f) for f in data.keys()])
        sql = "INSERT INTO {0} ({1}) VALUES ({2})".format(table, fields, fields_values)
        cursor.execute(sql, data)
        return cursor.lastrowid

    def create_history(self, data):
        """
        data = {
            'pKey': pkey,
            'tableName': 'inventory',
            'fieldName': i,
            'oldValue': 'CREATED',
            'newValue': data_copy.get(i),
        }
        """
        cursor = self._get_db().cursor()
        history = {
            "time": datetime.datetime.now(),
            "userName": "sqluser_{0}".format(self.user),
        }
        history.update(data)
        return self._insert("history", history)

    def _check_safe_to_create_or_update(self, data, current_pkey=None):
        has_identifiable_record = False
        # ensure no record with the same hostname, mac or ip found.. unless updating the same record
        for v in data.get("inv_dns", []):
            if v["hostDomainName"] and v["domain"]:
                if v["domain"] not in self.types["domain"]:
                    raise Exception(
                        "domain {0} not found in {0}".format(
                            v["domain"], self.types["domain"]
                        )
                    )
                fqdn = ".".join([v["hostDomainName"], v["domain"]])
                has_identifiable_record = True
                pkey = self._get_pkey_from_host(fqdn, False)
                if pkey and pkey != current_pkey:
                    raise Exception(
                        "existing record found with hostname {0}".format(fqdn)
                    )
            if v.get("macAddress"):
                m = self.format_mac(v["macAddress"])
                if m not in [mm["macAddress"] for mm in data.get("inv_mac", [])]:
                    raise Exception(
                        "inv_dns referances a mac {0} that is missing from inv_mac".format(
                            m
                        )
                    )
                has_identifiable_record = True
                pkey = self._get_pkey_from_mac(m)
                if pkey and pkey != current_pkey:
                    raise Exception("existing record found with mac {0}".format(m))
            if v.get("ipAddress"):
                # TODO: check format...
                has_identifiable_record = True
                pkey = self._get_pkey_from_ip(v["ipAddress"])
                if pkey and pkey != current_pkey:
                    raise Exception(
                        "existing record found with ip {0}".format(v["ipAddress"])
                    )
        for k in data.get("inv_mac", []):
            k = self.format_mac(k["macAddress"])
            has_identifiable_record = True
            pkey = self._get_pkey_from_mac(k)
            if pkey and pkey != current_pkey:
                raise Exception("existing record found with mac {0}".format(k))
        # TODO: should i keep support for host.... or move it to another lib...
        """
        for k in data['network']['host']:
            v = data['network']['host'][k]
            if self._get_pkey_from_host(k, False):
                raise Exception('existing record found with hostname {0}'.format(k))
            if v.get('mac') and self._get_pkey_from_mac(v['mac']):
                raise Exception('existing record found with mac {0}'.format(v['mac']))
            if v.get('ipv4addr') and self._get_pkey_from_ip(v['ipv4addr']):
                raise Exception('existing record found with ip {0}'.format(v['ipv4addr']))
        """
        # ensure if setting record inactive that supportClass is Retired
        if data.get("active"):
            if data["active"] == "n" and data.get("supportClass") != "Retired":
                raise Exception(
                    "when setting record inactive the supportClass must also be set to "
                )

        # ensure no active record with the same barcode
        if data.get("barcode"):
            has_identifiable_record = True
            pkey = self._get_pkey_from_barcode(data["barcode"])
            if pkey and pkey != current_pkey:
                raise Exception(
                    "existing record found with barcode {0}".format(data["barcode"])
                )

        # ensure somthing identifiable is set for the record
        if not has_identifiable_record:
            raise Exception("record needs somthing identifiable (dns or barcode)")

        # types are of a knowen type
        # TODO: this should be a db constraint but o well have to be safe
        if (
            data.get("equipmentType")
            and data.get("equipmentType") not in self.types["equipment"]
        ):
            raise Exception(
                "equipmentType {0} not found in {1}".format(
                    data.get("equipmentType"), self.types["equipment"]
                )
            )
        if data.get("groups") and data.get("groups") not in self.types["group"]:
            raise Exception(
                "group {0} not found in {1}".format(
                    data.get("groups"), self.types["group"]
                )
            )
        if data.get("special") and data.get("special") not in self.types["special"]:
            raise Exception(
                "special {0} not found in {1}".format(
                    data.get("special"), self.types["special"]
                )
            )
        if (
            data.get("supportClass")
            and data.get("supportClass") not in self.types["support"]
        ):
            raise Exception(
                "supportClass {0} not found in {1}".format(
                    data.get("supportClass"), self.types["support"]
                )
            )
        if (
            data.get("sponsorCode")
            and data.get("sponsorCode") not in self.types["sponsor"]
        ):
            raise Exception(
                "sponsorCode {0} not found in {1}".format(
                    data.get("sponsorCode"), self.types["sponsor"]
                )
            )

        return True

    def _set_defaults(self, data):
        if not "active" in data:
            data["active"] = "y"
        # if data.get('accountNumber') is None:
        #    # accountNumber is forced to be an empty string by the app not nill... perhaps not..
        #    data['accountNumber'] = ''
        if not "entered" in data:
            data["entered"] = datetime.datetime.now().date()
        return data

    def _create_inventory(self, data, update_history):
        """
        no checks!
        """
        data_copy = self._set_defaults({})
        for k in self.inventory_rw_items:
            if k in data:
                data_copy[k] = data[k]
        rowid = self._insert("inventory", data_copy)
        logger.debug("create_inventory record pkey: %s", rowid)
        if update_history:
            for i in data_copy.keys():
                if data_copy.get(i) and i not in ["pKey", "entered"]:
                    self.create_history(
                        {
                            "pKey": rowid,
                            "tableName": "inventory",
                            "fieldName": i,
                            "oldValue": "CREATED",
                            "newValue": data_copy.get(i),
                        }
                    )
        return rowid

    def _create_mac(self, pkey, macAddress, macName, update_history):
        """
        no checks but for a valid mac or null mac
        create the inventory mac item and return rowid
        """
        format_mac = self.format_mac(macAddress)
        rowid = self._insert(
            "inv_macAddress",
            {
                "pkey": pkey,
                "macAddress": format_mac,
                "macName": macName,
            },
        )
        if update_history:
            self.create_history(
                {
                    "pKey": pkey,
                    "tableName": "inv_macAddress",
                    "fieldName": "macAddress",
                    "oldValue": "CREATED",
                    "newValue": format_mac,
                }
            )
            if macName:
                self.create_history(
                    {
                        "pkey": pkey,
                        "tableName": "inv_macAddress",
                        "fieldName": "macName",
                        "oldValue": "CREATED",
                        "newValue": macName,
                    }
                )
        return rowid

    def _create_dns(
        self,
        pkey,
        hostDomainName,
        domain,
        ipAddress,
        macAddressKey,
        ipPurpose,
        syncWithDns,
        update_history,
    ):
        """
        no checks!

        create inventory dns item and return rowid
        """
        # if domain and domain not in self.get_domains():
        #    raise Exception('Domain {0} not found in list ov valid domains {1}'.format(domain, self,get_domains()))

        rowid = self._insert(
            "inv_dns",
            {
                "pkey": pkey,
                "hostDomainName": hostDomainName,
                "domain": domain,
                "ipAddress": ipAddress,
                "macAddressKey": macAddressKey,
                "ipPurpose": ipPurpose,
                "syncWithDns": syncWithDns,
            },
        )
        if update_history:
            self.create_history(
                {
                    "pkey": pkey,
                    "tableName": "inv_dns",
                    "fieldName": "hostDomainName",
                    "oldValue": "CREATED",
                    "newValue": hostDomainName,
                }
            )
            self.create_history(
                {
                    "pkey": pkey,
                    "tableName": "inv_dns",
                    "fieldName": "ipAddress",
                    "oldValue": "CREATED",
                    "newValue": ipAddress,
                }
            )
        return rowid

    def _create_licences(self, pkey, na):
        raise Exception("undefined")

    def _create_attachment(self, pkey, na):
        raise Exception("undefined")

    def _get_pkey_from_mac(self, mac, active=True):
        """
        this is a little risky because it is possible for two systems to have the same mac..
        TODO: check if more than one record found..
        TODO: perhaps need to check pkey is linked to an active record... fun fun.
        """
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        mac = self.format_mac(mac)
        if active == True:
            cursor.execute(
                "SELECT inv_macAddress.pKey FROM inv_macAddress INNER join inventory ON inventory.pKey=inv_macAddress.pKey WHERE inv_macAddress.macAddress = %s AND inventory.active = 'y'  LIMIT 1;",
                (mac,),
            )
        elif acrive == False:
            cursor.execute(
                "SELECT inv_macAddress.pKey FROM inv_macAddress INNER join inventory ON inventory.pKey=inv_macAddress.pKey WHERE inv_macAddress.macAddress = %s AND inventory.active = 'n'  LIMIT 1;",
                (mac,),
            )
        elif active == None:
            cursor.execute(
                "SELECT * from inv_macAddress WHERE inv_macAddress.macAddress = %s LIMIT 1;",
                (mac,),
            )
        data = cursor.fetchone()
        logger.debug("_get_pkey_from_mac %s", data)
        if data:
            return data["pKey"]
        logger.warning("_get_pkey_from_mac failed to find %s", mac)

    def _get_pkey_from_host(self, host, search_host_without_domain, active=True):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        ## GET pkey use fqdn first then try other methods
        logger.debug("_get_pkey_from_host %s", host)
        if active == True:
            cursor.execute(
                "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE concat(inv_dns.hostDomainName,'.',inv_dns.domain) = %s AND inventory.active = 'y'  LIMIT 1;",
                (host,),
            )
        elif acrive == False:
            cursor.execute(
                "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE concat(inv_dns.hostDomainName,'.',inv_dns.domain) = %s AND inventory.active = 'n'  LIMIT 1;",
                (host,),
            )
        elif active == None:
            cursor.execute(
                "SELECT inv_dns.pKey FROM inv_dns WHERE concat(inv_dns.hostDomainName,'.',inv_dns.domain) = %s LIMIT 1;",
                (host,),
            )
        data = cursor.fetchone()
        if not data and search_host_without_domain:
            if active == True:
                cursor.execute(
                    "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.hostDomainName = %s AND inventory.active = 'y'  LIMIT 1;",
                    (host,),
                )
            elif acrive == False:
                cursor.execute(
                    "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.hostDomainName = %s AND inventory.active = 'n'  LIMIT 1;",
                    (host,),
                )
            elif active == None:
                cursor.execute(
                    "SELECT inv_dns.pKey FROM inv_dns WHERE inv_dns.hostDomainName = %s LIMIT 1;",
                    (host,),
                )
            data = cursor.fetchone()
            if not data and host.find("."):
                if active == True:
                    cursor.execute(
                        "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.hostDomainName = %s AND inventory.active = 'y'  LIMIT 1;",
                        (host.split(".")[0],),
                    )
                elif acrive == False:
                    cursor.execute(
                        "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.hostDomainName = %s AND inventory.active = 'n'  LIMIT 1;",
                        (host.split(".")[0],),
                    )
                elif active == None:
                    cursor.execute(
                        "SELECT inv_dns.pKey FROM inv_dns WHERE inv_dns.hostDomainName = %s LIMIT 1;",
                        (host.split(".")[0],),
                    )
                data = cursor.fetchone()
        logger.debug("_get_pkey_from_host %s", data)
        if data:
            return data["pKey"]
        logger.warning("_get_pkey_from_host failed to find %s", host)

    def _get_pkey_from_ip(self, ip, active=True):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        if active == True:
            cursor.execute(
                "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.ipAddress = %s AND inventory.active = 'y'  LIMIT 1;",
                (ip,),
            )
        elif acrive == False:
            cursor.execute(
                "SELECT inv_dns.pKey FROM inv_dns INNER join inventory ON inventory.pKey=inv_dns.pKey WHERE inv_dns.ipAddress = %s AND inventory.active = 'n' LIMIT 1;",
                (ip,),
            )
        elif active == None:
            cursor.execute("SELECT * FROM inv_dns WHERE ipAddress = %s LIMIT 1;", (ip,))
        data = cursor.fetchone()
        logger.debug("_get_pkey_from_ip %s", data)
        if data:
            return data["pKey"]
        logger.warning("_get_pkey_from_ip failed to find %s", ip)

    def _get_pkey_from_barcode(self, barcode, active=True):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        if active == True:
            cursor.execute(
                "SELECT pKey FROM inventory WHERE barcode = %s AND active='y' LIMIT 1;",
                (barcode,),
            )
        elif acrive == False:
            cursor.execute(
                "SELECT pKey FROM inventory WHERE barcode = %s AND active='n' LIMIT 1;",
                (barcode,),
            )
        elif acrive == None:
            cursor.execute(
                "SELECT pKey FROM inventory WHERE barcode = %s LIMIT 1;", (barcode,)
            )
        data = cursor.fetchone()
        logger.debug("_get_pkey_from_barcode %s", data)
        if data:
            return data["pKey"]
        logger.warning("_get_pkey_from_barcode failed to find %s", barcode)

    def _get_inventory_from_pkey(
        self, pkey, include_network=True, include_history=False, active=True
    ):
        ## GET RECORD
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        if active == True:
            cursor.execute(
                "SELECT * FROM inventory WHERE pKey = %s AND active='y' LIMIT 1;",
                (pkey,),
            )
        elif active == False:
            cursor.execute(
                "SELECT * FROM inventory WHERE pKey = %s AND active='n' LIMIT 1;",
                (pkey,),
            )
        elif active == None:
            cursor.execute("SELECT * FROM inventory WHERE pKey = %s LIMIT 1;", (pkey,))
        data = cursor.fetchone()
        if not data:
            logger.warning("Unable to find an active record for pkey:{0}".format(pkey))
            return None
        if include_network:
            network = self._get_inventory_network_from_pkey(pkey)
            if network:
                data.update(network)
        if include_history:
            raise "TODO"
        return data

    def _get_inventory_network_from_pkey(self, pkey):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        data = {}
        inv_macAddress = {}
        data["inv_mac"] = []
        data["inv_dns"] = []
        logger.debug("_get_inventory_network_from_pkey")
        cursor.execute(
            "SELECT * from inv_macAddress WHERE inv_macAddress.pKey = %s;", (pkey,)
        )
        for mac in cursor:
            macAddress = self.format_mac(mac["macAddress"])
            inv_macAddress[mac["id"]] = macAddress
            data["inv_mac"].append(
                {
                    "macAddress": macAddress,
                    "macName": mac["macName"],
                }
            )
        cursor.execute("SELECT * from inv_dns WHERE inv_dns.pKey = %s;", (pkey,))
        for dns in cursor:
            # name = dns['id']
            # if dns['hostDomainName'] and dns['domain']:
            #    name = '{0}.{1}'.format(dns['hostDomainName'], dns['domain'])
            #    #if 'primary_fqdn' not in data:
            #    #    # WARN: requires records sorted so only the first record is used for primary name
            #    #    data['primary_fqdn'] = name
            macAddress = None
            if dns["macAddressKey"] in inv_macAddress:
                macAddress = self.format_mac(inv_macAddress[dns["macAddressKey"]])

            data["inv_dns"].append(
                {
                    "hostDomainName": dns["hostDomainName"],
                    "domain": dns["domain"],
                    "ipPurpose": dns["ipPurpose"],
                    "ipAddress": dns["ipAddress"],
                    "macAddress": macAddress,
                }
            )

            # TODO: if type host:
            # if fqdn:
            #    data['host'][fqdn] = {
            #        'description': dns['ipPurpose'],
            #        'ipv4addr': dns['ipAddress'],
            #        'mac': macAddress,
            #    }

            # TODO: if type cname:
        return data

    # ===============================================================================================================
    # ===============================================================================================================
    # ===============================================================================================================

    def get_domains(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_domain;")
        for d in cursor:
            i.append(d["domain"])
        return i

    def get_equipment_types(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_equipment_type;")
        for d in cursor:
            i.append(d["value"])
        return i

    def get_groups(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_groups;")
        for d in cursor:
            i.append(d["gr_name"])
        return i

    def get_specials(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_special;")
        for d in cursor:
            i.append(d["value"])
        return i

    def get_support_classes(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_support_class;")
        for d in cursor:
            i.append(d["value"])
        return i

    def get_sponsors(self):
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        i = []
        cursor.execute("select * from aux_sponsors;")
        for d in cursor:
            i.append(d["code"])
        return i

    def get(
        self,
        pkey=None,
        host=None,
        mac=None,
        ip=None,
        barcode=None,
        search_host_without_domain=True,
        include_network=True,
        include_history=False,
        active=True,
        strip_empty_values=True,
    ):
        """
        like get_inventory but uses a cache
        """
        d = None
        cache_key = "{0},{1},{2},{3}".format(pkey, host, mac, ip)
        try:
            d = self.cache[cache_key]
            # if d is None:
            #    if self.cache.ttl(key) < 500:
            #        raise Exception('Reduce ttl for unresolved inventory')
            logger.debug("using cached record: %s", cache_key)
        except Exception as ex:
            logger.debug("raw lookup: for cache %s", cache_key)
            d = self.get_inventory(
                pkey,
                host,
                mac,
                ip,
                barcode,
                search_host_without_domain,
                include_network,
                include_history,
                active,
                strip_empty_values,
            )
            self.cache[cache_key] = d
        return d

    def get_inventory(
        self,
        pkey=None,
        host=None,
        mac=None,
        ip=None,
        barcode=None,
        search_host_without_domain=True,
        include_network=True,
        include_history=False,
        active=True,
        strip_empty_values=True,
    ):
        """
        return the inventory record
        """
        logger.info(
            "_get_inventory: using one of (host=%s mac=%s ip=%s barcode=%s)",
            host,
            mac,
            ip,
            barcode,
        )
        if pkey:
            pass
        elif host:
            pkey = self._get_pkey_from_host(host, search_host_without_domain, active)
        elif mac:
            pkey = self._get_pkey_from_mac(mac, active)
        elif ip:
            pkey = self._get_pkey_from_ip(ip, active)
        elif barcode:
            pkey = self._get_pkey_from_barcode(barcode, active)

        if pkey:
            data = self._get_inventory_from_pkey(
                pkey, include_network, include_history, active
            )
            if strip_empty_values:
                data = self.strip_empty_values(data)
            return data
        return None

    def create(self, data, update_history=True):
        """
        safly create a record
        """
        # could be a race if two records get created at same time with same hostname.
        if not self._check_safe_to_create_or_update(data):
            return None

        db = self._get_db()
        db.autocommit = False
        pkey = None

        try:
            # create the inventory record first because need a pkey in the mac_tbl and dns_tbl
            pkey = self._create_inventory(data, update_history)
            if data.get("network", {}):
                pass
                """
                # TODO: should i keep support for network.... or move it to another lib...
                # if no old format is used then allow host format...
                # create the inventory dns using helper (host) item(s)
                for k in data.get('network', {}).get('host', {}):
                    v = data['network']['host'][k]
                    format_mac = self.format_mac(v.get('mac'))
                    domains = self.get_domains()
                    hostDomainName = parse_hostname(k, domains)
                    domain = parse_hostname(k, domains)
                    macAddressKey = inv_macAddress.get(format_mac, None)
                    rowid = self._create_dns(
                        pkey=pkey,
                        hostDomainName=hostDomainName,
                        domain=domain,
                        ipAddress=v['ipv4addr'],
                        macAddressKey=macAddressKey,
                        ipPurpose=v['description'],
                        syncWithDns=0,
                        update_history=True,
                    )
                """
            else:
                # create the inventory mac item(s)
                # track of the mac rowid because needed in the dns record
                macAddressKey_map = {}
                for v in data.get("inv_mac", []):
                    format_mac = self.format_mac(v["macAddress"])
                    rowid = self._create_mac(
                        pkey=pkey,
                        macAddress=format_mac,
                        macName=v["macName"],
                        update_history=update_history,
                    )
                    macAddressKey_map[format_mac] = rowid

                # create the inventory dns raw format item(s)
                for v in data.get("inv_dns", []):
                    format_mac = self.format_mac(v.get("macAddress"))
                    macAddressKey = macAddressKey_map.get(format_mac)
                    rowid = self._create_dns(
                        pkey=pkey,
                        hostDomainName=v.get("hostDomainName"),
                        domain=v.get("domain"),
                        ipAddress=v.get("ipAddress"),
                        macAddressKey=macAddressKey,
                        ipPurpose=v.get("ipPurpose"),
                        syncWithDns=0,
                        update_history=True,
                    )
            # TODO: perhaps just clear the changed item
            self.cache.clear()
        except Exception as ex:
            db.rollback()
            pkey = None
            logger.exception("Issue with the inventory commit. Record rollbacked")
            raise ex
        finally:
            db.autocommit = True
        return pkey

    def strip_empty_values(self, data, ignore_keys=[]):
        """
        returns a copy of an object without keys with empty(None) values.
        """
        if data == None:
            return None
        # if isinstance(data, OrderedDict):
        #    data = dict(data)
        if isinstance(data, dict):
            r = {}
            for k in data:
                if k in ignore_keys:
                    r[k] = f
                else:
                    f = self.strip_empty_values(data.get(k))
                    if f != None:
                        r[k] = f
            return r
        elif isinstance(data, list):
            r = []
            for d in data:
                f = self.strip_empty_values(d)
                if f != None:
                    r.append(f)
            return r
        return data

    def find_record_changes(
        self,
        a,
        b,
        strip_empty=True,
        populate_inventory_rw_item=True,
        strip_inventory_ro_item=True,
        verbose_level=1,
        ignore_order=False,
        ignore_type_changes=True,
    ):
        """
        a=old
        b=new
        """
        aa = {}
        bb = {}
        if populate_inventory_rw_item:
            aa.update(self.inventory_rw_item_dict)
            bb.update(self.inventory_rw_item_dict)
        aa = self._set_defaults(aa)
        bb = self._set_defaults(bb)
        if strip_empty:
            aa.update(self.strip_empty_values(a.copy()))
            bb.update(self.strip_empty_values(b.copy()))
        else:
            aa.update(a)
            bb.update(b)
        if strip_inventory_ro_item:
            for k in self.inventory_ro_items:
                aa.pop(k, None)
                bb.pop(k, None)

        diff = DeepDiff(aa, bb, ignore_order=ignore_order, verbose_level=verbose_level)
        if diff and verbose_level == 0 and ignore_type_changes:
            blacklist_typechange = ["root['inv_dns']", "root['inv_mac']"]
            if "type_changes" in diff:
                for k in diff.get("type_changes", {}):
                    if k in blacklist_typechange:
                        # return the full diff is one of the blacklisted change are found
                        return diff
            diff.pop("type_changes", None)
        return diff

    def update(self, pkey, data, update_history=True):
        """
        Safly update a record
        """
        data = data.copy()
        data = self._set_defaults(data)

        if not self._check_safe_to_create_or_update(data, pkey):
            return None
        db = self._get_db()
        cursor = db.cursor(dictionary=True, buffered=True)
        db.autocommit = False

        logger.debug("update pkey {0} with {1}".format(pkey, data))

        # update changes in record
        updates = {}
        try:
            iold = self._get_inventory_from_pkey(pkey)
            if not iold:
                raise Exception("No record found to update using pkey {0}".format(pkey))
            if not self.find_record_changes(iold, data):
                # no changes found between the records...
                return None

            ### -- update inventory table
            blocked_lower = ["inv_dns", "inv_mac"]
            blocked_lower.extend(self.inventory_ro_items)
            blocked_lower = [a.lower() for a in blocked_lower]
            for i in data:
                if i.lower() not in blocked_lower:
                    if not data.get(i) == iold.get(i):
                        logger.debug(
                            "UPDATE inventory SET %s = %s WHERE pkey = %s"
                            % (
                                i,
                                data.get(i),
                                pkey,
                            )
                        )
                        cursor.execute(
                            "UPDATE inventory SET {0}= %s WHERE pkey = %s".format(i),
                            (
                                data.get(i),
                                pkey,
                            ),
                        )
                        updates[i] = "{0} ---> {1}".format(iold.get(i), data.get(i))
                        if update_history:
                            self.create_history(
                                {
                                    "pkey": pkey,
                                    "tableName": "inventory",
                                    "fieldName": i,
                                    "oldValue": iold.get(i),
                                    "newValue": data.get(i),
                                }
                            )

            ### -- update inv_mac and inv_dns tables if needed
            # mac deletes dont create history
            # create mac create history
            ## mac addresses are needed for dns items so create them first if not already found
            # for m in inv_macAddress

            # dns deletes dont create history
            # create dns create history

            c = DeepDiff(
                self.strip_empty_values(iold.get("inv_mac", [])),
                self.strip_empty_values(data.get("inv_mac", [])),
                ignore_order=True,
            )
            if c:
                updates["inv_mac"] = str(c)

            c = DeepDiff(
                self.strip_empty_values(iold.get("inv_dns", [])),
                self.strip_empty_values(data.get("inv_dns", [])),
                ignore_order=True,
            )
            if c:
                updates["inv_dns"] = str(c)

            if updates.get("inv_mac") or updates.get("inv_dns"):
                ## rebuild all dns records for this pkey
                self.delete(pkey, delete_from=["inv_macAddress", "inv_dns"])
                if update_history:
                    self.create_history(
                        {
                            "pkey": pkey,
                            "tableName": "inv_macAddress",
                            "fieldName": "inv_macAddress.*",
                            "oldValue": "DELETE",
                            "newValue": "",
                        }
                    )
                    self.create_history(
                        {
                            "pkey": pkey,
                            "tableName": "inv_dns",
                            "fieldName": "inv_dns.*",
                            "oldValue": "DELETE",
                            "newValue": "",
                        }
                    )
                # create the inventory mac item(s)
                # track mac rowid because needed in the dns record
                macAddressKey_map = {}
                for v in data.get("inv_mac", []):
                    format_mac = self.format_mac(v["macAddress"])
                    rowid = self._create_mac(
                        pkey=pkey,
                        macAddress=format_mac,
                        macName=v["macName"],
                        update_history=update_history,
                    )
                    macAddressKey_map[format_mac] = rowid

                # create the inventory dns raw format item(s)
                for v in data.get("inv_dns", []):
                    format_mac = self.format_mac(v.get("macAddress"))
                    macAddressKey = macAddressKey_map.get(format_mac)
                    rowid = self._create_dns(
                        pkey=pkey,
                        hostDomainName=v.get("hostDomainName"),
                        domain=v.get("domain"),
                        ipAddress=v.get("ipAddress"),
                        macAddressKey=macAddressKey,
                        ipPurpose=v.get("ipPurpose"),
                        syncWithDns=0,
                        update_history=True,
                    )

            ### -- update history on inv_mac and inv_dns changes
            """
            if update_history:
                # TODO: empty field name and oldvalue is not the best...
                if updates.get('inv_mac'):
                    self.create_history({
                        'pkey': pkey,
                        'tableName': 'inventory',
                        'fieldName': 'NA',
                        'oldValue': 'NA',
                        'newValue': updates.get('inv_mac'),
                    })
                if updates.get('inv_dns'):
                    self.create_history({
                        'pkey': pkey,
                        'tableName': 'inventory',
                        'fieldName': 'NA',
                        'oldValue': 'NA',
                        'newValue': updates.get('inv_dns'),
                    })
            """

            # db.commit()
            # TODO: perhaps just clear the changed item
            self.cache.clear()
        except Exception as ex:
            db.rollback()
            updates = {}
            logger.exception("Issue with the inventory commit. Record rollbacked")
            raise ex
        finally:
            db.autocommit = True

        return updates

    def format_mac(self, mac, delimiter=":"):
        if not mac:
            return mac
        macf = mac.lower().replace(":", "").replace("-", "").replace(".", "")
        if not len(macf) == 12:
            raise Exception("mac address {0} is not 12 hex long".format(mac))
        macf = delimiter.join(macf[i : i + 2] for i in range(0, 12, 2))
        if mac != macf:
            logger.warning("format_mac changed mac {0} to {1}".format(mac, macf))
        return macf

    def delete_loans(self, barcode):
        db = self._get_db()
        cursor = db.cursor(buffered=True)
        cursor.execute("DELETE FROM loan WHERE barcode = %s".format(k), (barcode,))

    def delete(
        self,
        pkey,
        delete_from=[
            "attachments",
            "licences",
            "history",
            "inv_macAddress",
            "inv_dns",
            "inventory",
        ],
    ):
        db = self._get_db()
        cursor = db.cursor(buffered=True)
        for k in delete_from:
            cursor.execute("DELETE FROM {0} WHERE pKey = %s".format(k), (pkey,))


# logger.setLevel(logging.DEBUG)
## TEST
if __name__ == "__main__":
    logger.setLevel(logging.DEBUG)
    a = Inventory("database.math", "mfcfsalt", "xxxx", "app_inventory")
    d = a.get_inventory(mac="00:25:90:5a:79:2c")
    d = a.get(mac="00:25:90:5a:79:2c")
    d = a.get(mac="00:25:90:5a:79:2c")
    print(d)
