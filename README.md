
install
--------------

```
## install setup tools and mysql libs, using what works for you.
apt install python-setuptools libmysqlclient-dev libpython2.7-dev
yum install python-setuptools mysql-devel python-devel
python -m pip install setuptools
python3 -m pip install setuptools

## install package using your version of python.
python -m pip install https://git.uwaterloo.ca/mfcf/mfcfinventory/raw/master/dist/mfcfinventory-0.3.tar.gz
python3 -m pip install https://git.uwaterloo.ca/mfcf/mfcfinventory/raw/master/dist/mfcfinventory-0.3.tar.gz
```


install from source
--------------

```
python3 setup.py install

# or
apt install libmysqlclient-dev
pip3 install dist/mfcfinventory-0.3.tar.gz
pip install -I dist/mfcfinventory-0.3.tar.gz
```

developer
--------------

to recreate package

```
# tar.gz
python3 setup.py sdist

# .egg
?
```


version
---------

0.3: added field isSaltProvisioned
