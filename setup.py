#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="mfcfinventory",
    version="0.5",
    description="Access inventory",
    author="Steve Weber",
    author_email="s8weber@uwaterloo.ca",
    packages=find_packages(),
    install_requires=["mysql-connector", "deepdiff"],
)
# python-pymysql
